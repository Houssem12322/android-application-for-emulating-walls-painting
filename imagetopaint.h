#ifndef IMAGETOPAINT_H
#define IMAGETOPAINT_H

#include <QtQuick/QQuickPaintedItem>
#include <QColor>
#include <QImage>
#include "walls.h"
#include "QDebug"
#include "optimized.h"
#include "QThread"

class imageToPaint : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(bool textureLoaded READ textureLoaded)
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QColor color READ color WRITE setColor)
    Q_PROPERTY(QString path READ path WRITE setPath)

public:
    imageToPaint(QQuickItem *parent = 0);
    ~imageToPaint();

    bool textureLoaded();

    QString path() const;
    void setPath(const QString &path);

    QString name() const;
    void setName(const QString &name);

    QColor color() const;
    void setColor(const QColor &color);

    Q_INVOKABLE void loadWallsData(float width, float height);
    Q_INVOKABLE void paintWall(float x, float y, const QColor &paintColor, QString path);
    Q_INVOKABLE void loadTexture(QString texturePath);
    Q_INVOKABLE void removePainting(int wallNumber);

    void paint(QPainter *painter);


public slots:
    void loadStat(const bool stat);


signals:
    void startLoad(QImage *img, QString path);
    void wallColored(int wallNmber, float x, float y);

private:
    float m_widthRatio;
    float m_heightRatio;
    bool m_textureLoaded;
    QString m_name;
    QColor m_color;
    QString m_path;
    QImage m_original;
    QImage m_copy;
    QImage m_texture;
    Walls wallsTab[6];
    QThread load;
    Optimized *m_parallelProcess;

};
#endif // IMAGETOPAINT_H
