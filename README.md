This application aims to simulate the process of painting walls with simple paints or textures. 
It was developed using Qt framework (QML and C++).

To build this project smoothly you need to use Qt 5.11 for android. documentation to install and configure Qt 5.11 for android can be found in 'http://doc.qt.io/qt-5/androidgs.html'.

REMARK: the file images.rcc is an external binary resources created using rcc Qt tool (Resource Compiler). it contains resources needed by the application.

The application can be found in play store 'Opera Peinture' using the link 'https://play.google.com/store/apps/details?id=org.qtproject.opera_peinture'

For more information feel free to contact me on my email adress 'houssem.merdaci@gmail.com' 