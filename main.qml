import QtQuick 2.11
import QtQuick.Layouts 1.6
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.Universal 2.4
import Qt.labs.settings 1.0
import "components"
import Paint 1.0
import QtGraphicalEffects 1.0


ApplicationWindow {
    // The main window
    id: window
    width: 680
    height: 1024
    visible: true
    title: "Opera"
    visibility: "FullScreen"

    Rectangle{
        // Splash
        id: splach
        z:101
        anchors.fill: parent
        color: "white"


        Image {
            id:operaLogo
            width: parent.width /2
            fillMode: Image.PreserveAspectFit

            x: -parent.height
            y: (splach.height-height)/3
            source: "qrc:/images/operaLogo.png"
        }
        DirectionalBlur {
                id:blur
                anchors.fill: operaLogo
                source: operaLogo
                angle: 90
                length: 32
                samples: 24
            }

            DropShadow {
                    anchors.fill: operaLogo
                    horizontalOffset: 0
                    verticalOffset: 10
                    radius: 5.0
                    samples: 17
                    color: "#aa000000"
                    source: operaLogo
                }

        SequentialAnimation
        {
            id: logoAnimation

            PropertyAnimation {
            target: operaLogo;
            property: "x";
            to: (splach.width - operaLogo.width)/2;
            duration: 2000
            easing.type: Easing.OutElastic;

            }
            PropertyAnimation {
            target: blur;
            property: "length";
            to: 0.0;
            duration: 1000

            }
            PropertyAnimation {
            target: splach;
            property: "opacity";
            to: 0.0;
            duration: 4000
            easing.type: Easing.InExpo

            }

            onStopped: {
                pane.enabled =true
            }

        }

        AnimatedImage{
            id: wordsAnimation
            width: parent.width * 2/3
            anchors.top: operaLogo.bottom
            anchors.topMargin: 10

            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
            source: "qrc:/videos/operaWords.gif"
        }

        Component.onCompleted: {
            logoAnimation.running = true;
            pane.enabled =false
        }

    }


    Settings {
        id: settings
        property string style: "Default"
    }

    Shortcut {
        id: escBack
        sequences: ["Esc", "Back"]
        enabled: stackView.depth > 1
        onActivated: {

            toolBar.visible = true
            stackView.pop()
            listView.currentIndex = -1
            titleLabel.text = Qt.binding(function() { return langue.now["home"] })

        }
    }

    Shortcut {
        sequence: "Menu"
        onActivated: optionsMenu.open()
    }


    ToolBar {
        id:toolBar
        Material.foreground: "#757575"
        Material.background: "#ecffffff"
        width: parent.width
        z:100

        RowLayout {
            spacing: 20
            anchors.fill: parent

            ToolButton {
                icon.name: stackView.depth > 1 ? "back" : "drawer"
                onClicked: {
                    if (stackView.depth > 1) {
                        stackView.pop()
                        listView.currentIndex = -1
                        titleLabel.text = Qt.binding(function() { return langue.now["home"] })
                        backB.visible = false
                        forwardB.visible = false
                    } else {
                        drawer.open()
                    }
                }
            }
            ToolButton {
                id: backB
                visible: false
                icon.source: "qrc:/icons/back.png"
                onClicked: {

                       stackView.currentItem.back()
                }

            }

            Label {
                id: titleLabel
                text: langue.now["home"]
                font.pixelSize: 20
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
            ToolButton {
                id: forwardB
                visible: false
                icon.source: "qrc:/icons/forward.png"
                onClicked: {

                       stackView.currentItem.forward()

                }

            }

            ToolButton {
                icon.name: "menu"
                onClicked: optionsMenu.open()

                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    MenuItem {
                        text: langue.now["languages"]
                        onTriggered: settingsDialog.open()
                    }
                    MenuItem {
                        text: langue.now["about"]
                        onTriggered: aboutDialog.open()
                    }
                }
            }
        }
    }

    Drawer {
        id: drawer
        width: Math.min(window.width, window.height) / 3 * 2
        height: window.height
        interactive: stackView.depth === 1

        ListView {
            id: listView

            focus: true
            currentIndex: -1
            anchors.fill: parent

            delegate: ItemDelegate {
                width: parent.width
                text: langue.now[model.title]
                highlighted: ListView.isCurrentItem
                onClicked: {
                    listView.currentIndex = index
                    stackView.push(model.source)
                    titleLabel.text = Qt.binding(function() { return langue.now[model.title] })

                    if(model.title === "videos")
                    {
                        backB.visible = true
                        forwardB.visible = true
                    }

                    drawer.close()
                }
            }

            model: ListModel {
                //ListElement { title: "Home"; source: "qrc:/main.qml" }
                ListElement { title: "painting"; source: "qrc:/pages/WallsPainting.qml" }
                //ListElement { title: "catalog"; source: "qrc:/pages/Catalog.qml" }
                ListElement { title: "videos"; source: "qrc:/pages/Videos.qml" }
                ListElement { title: "contact"; source: "qrc:/pages/Contacts.qml" }

            }

            ScrollIndicator.vertical: ScrollIndicator { }


        }
    }

    StackView {
        Language{
            id: langue
        }

        id: stackView
        focus: true
        anchors.fill: parent


        initialItem:Flickable {
            id: pane

            contentWidth: buttonsColumn.width
            contentHeight:  buttonsColumn.height

            boundsMovement: Flickable.StopAtBounds
            boundsBehavior: Flickable.DragOverBounds
            opacity: Math.max(0.5, 1.0 - Math.abs(verticalOvershoot) / height)


            Column{
                id: buttonsColumn
                spacing: 0
                width: stackView.width
                Image {

                    id: backgroundIm
                    source: "images/opBack2.png"
                    width: parent.width
                    fillMode: Image.PreserveAspectCrop
                    height: stackView.height * 3/5
                    Rectangle{
                        anchors.fill: parent
                        color: "#0f000000"
                    }

                    Column
                    {
                        anchors.centerIn: parent
                        spacing: 10

                        Text{
                            color: "white"
                            text: qsTr(langue.now["forMore"])
                            font.family: "Kartika"
                            font.bold: true
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: 22
                            verticalAlignment: Text.AlignVCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        Rectangle
                        {
                            width: backgroundIm.width/2
                            height: backgroundIm.width/4
                            anchors.horizontalCenter: parent.horizontalCenter
                            color: clickablLogo.pressed? "#90ffffff":"#80ffffff"
                            border.width: 1
                            border.color: "white"
                            radius: 10
                            Image {
                                id: logoButton
                                anchors.centerIn: parent
                                source: "qrc:/images/operaLogo.png"
                                fillMode: Image.PreserveAspectFit
                                width: clickablLogo.pressed ? backgroundIm.width/2 -10 : backgroundIm.width/2
                                MouseArea{
                                    id: clickablLogo
                                    anchors.fill: parent
                                    onClicked:{
                                        Qt.openUrlExternally("http://operapeinture.com");
                                    }
                                }

                            }
                        }




                     }

                }
                BigButton{
                    bColor: "#f0f0f0"
                    bWidth: parent.width
                    bHeight: stackView.height/4
                    buttonLabel: langue.now["paintB"]
                    path: "qrc:images/paint3.png"
                    onClicked: {
                        titleLabel.text = Qt.binding(function() { return langue.now["painting"] })
                        stackView.push("qrc:/pages/WallsPainting.qml")
                    }
                }


                BigButton{
                    bWidth: parent.width
                    bHeight: stackView.height/4
                    buttonLabel: langue.now["videoB"]
                    path: "qrc:images/video3.png"
                    onClicked: {
                        titleLabel.text = Qt.binding(function() { return langue.now["videos"] })
                        stackView.push("qrc:/pages/Videos.qml")
                        backB.visible = true
                        forwardB.visible = true
                    }

                }

                /*BigButton{
                    bColor: "#f0f0f0"
                    bWidth: parent.width
                    bHeight: stackView.height/4
                    buttonLabel: langue.now["catalogB"]
                    path: "qrc:images/catalog3.png"
                    onClicked: {
                        titleLabel.text = Qt.binding(function() { return langue.now["catalog"] })
                        stackView.push("qrc:/pages/Catalog.qml")
                    }

                }*/

                BigButton{
                    bWidth: parent.width
                    bHeight: stackView.height/4
                    buttonLabel: langue.now["contactB"]
                    path: "qrc:images/location3.png"
                    onClicked: {
                        titleLabel.text = Qt.binding(function() { return langue.now["contact"] })
                        stackView.push("qrc:/pages/Contacts.qml")
                    }

                }

            }
        }


    }

    Dialog {
        id: settingsDialog
        x: Math.round((window.width - width) / 2)
        y: Math.round(window.height / 6)
        width: Math.round(Math.min(window.width, window.height) / 3 * 2)
        modal: true
        focus: true
        title: langue.now["languages"]

        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            switch (langueBox.currentText)
            {
                case "Français":
                    langue.now = langue.fr
                    break;

                case "English":
                    langue.now = langue.en
                    break;

                case "عربية":
                    langue.now = langue.ar
                    break;
            }


            settingsDialog.close()
        }
        onRejected: {

            settingsDialog.close()
        }

        contentItem: ColumnLayout {
            id: settingsColumn
            spacing: 20

            RowLayout {
                spacing: 10

                Label {
                    text: langue.now["languages"]
                }

                ComboBox {
                    id: langueBox
                    property int styleIndex: -1
                    model: ["Français", "English", "عربية"]
                    currentIndex: 0
                    Layout.fillWidth: true
                }
            }

        }
    }

    Dialog {
        id: aboutDialog
        modal: true
        focus: true
        title:langue.now["about"]
        x: (window.width - width) / 2
        y: window.height / 6
        width: Math.min(window.width, window.height) / 3 * 2
        contentHeight: aboutColumn.height

        Column {
            id: aboutColumn
            spacing: 10

            Image {
                id: scriptihaLogo
                source: "qrc:/images/scriptihaLogo.png"
                width:parent.width/4
                fillMode: Image.PreserveAspectFit
            }
            Label {
                width: aboutDialog.availableWidth
                text: langue.now["aboutContent"]
                wrapMode: Label.Wrap
                font.pixelSize: 12
            }
        }
    }
}
