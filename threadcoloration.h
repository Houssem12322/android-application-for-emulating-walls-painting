#ifndef THREADCOLORATION_H
#define THREADCOLORATION_H
#include"QThread"
#include"QImage"

class ThreadColoration : public QThread
{
    Q_OBJECT
public:
    ThreadColoration();
    void set(QImage *tImg, QImage *tWall, QImage *tCopy, QColor paintColor, int y, int m_y, int m_x, int linePerThread, int threadNumber, bool *threadStat);
    void setTexture(QImage *tImg, QImage *tWall, QImage *tTexture, QImage *tCopy, int tY, int m_tY, int m_tX, int tLinePerThread, int tThreadNumber, bool *tThreadStat, float *tTransformation);
    void setToRemovePainting(QImage *tImg, QImage *tWall, QImage *tCopy, int tY, int m_tY, int m_tX, int tLinePerThread, int tThreadNumber, bool *tThreadStat);
    void run() override;

private:
    QImage *img;
    QImage *texture;
    QImage *wall;
    QImage *copy;
    QColor paintColor;
    int y;
    int m_y;
    int m_x;
    int linePerThread;
    int threadNumber;
    int colorationOrTexture;
    bool *threadStat;
    float m_oneDivthree;
    float m_gama;
    float m_100D255;
    float *transformation;

    void getAB(const QColor &colorTopaint, float &a, float &b);
    void getL(const QColor &imageColor, float &l);
    QColor rgbFromlab(float const &l, float const &a, float const &b);

};

#endif // THREADCOLORATION_H
