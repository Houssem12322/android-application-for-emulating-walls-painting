#ifndef OPTIMIZED_H
#define OPTIMIZED_H

#include <QObject>
#include "QImage"


class Optimized : public QObject
{
    Q_OBJECT
public:
    explicit Optimized(QObject *parent = nullptr);

signals:
    void finish(const bool stat);

public slots:
    void LoadImage(QImage *img, QString path);


};

#endif // OPTIMIZED_H
