#include "imagetopaint.h"
#include "QPainter"
#include <QtQuick/QQuickPaintedItem>

imageToPaint::imageToPaint(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
    m_textureLoaded = false;
    m_parallelProcess = new Optimized;
    m_parallelProcess->moveToThread(&load);
    connect(&load, &QThread::finished, m_parallelProcess, &QObject::deleteLater);
    connect(this, &imageToPaint::startLoad, m_parallelProcess, &Optimized::LoadImage);
    connect(m_parallelProcess, &Optimized::finish, this, &imageToPaint::loadStat);
    load.start();
}

imageToPaint::~imageToPaint() {
        load.quit();
        load.wait();
    }

bool imageToPaint::textureLoaded()
{
    return m_textureLoaded;
}

QString imageToPaint::path() const
{
    return m_path;

}

void imageToPaint::setPath(const QString &path)
{
    m_path = path;
    if(!m_original.load(path + ".jpeg", "JPG"))
    {
        qDebug()<<"error when loading the image\n";
        exit(0);
    }
    m_copy = m_original.copy();

}

void imageToPaint::loadWallsData(float width, float height)
{
    m_widthRatio =1920.0 / width;
    m_heightRatio = 1080.0 / height;

    for(int i=0; i<6; i++)
    {
        wallsTab[i].loadData(m_path,i+1);
    }
}

void imageToPaint::loadTexture( QString texturePath)
{
    emit startLoad(&m_texture, texturePath);
}


void imageToPaint::paintWall(float x, float y, const QColor &paintColor, QString path)
{
    int realX = (int)round(x * m_widthRatio);
    int realY = (int)round(y * m_heightRatio);

    for(int i=0; i<6; i++)
    {
        if(wallsTab[i].m_exists && wallsTab[i].inWall(realX, realY))
        {
            if(path == "")
            {
                wallsTab[i].fillInImage(m_original, m_copy, paintColor);
                emit wallColored(i, x, y);
                update();
            }
            else
            {
                if(m_textureLoaded)
                {
                    if(wallsTab[i].fillTexure(m_original, m_texture, m_copy))
                    {
                        emit wallColored(i, x, y);
                        update();
                    }
                }
            }

            break;
        }
    }

}

QString imageToPaint::name() const
{
    return m_name;
}

void imageToPaint::setName(const QString &name)
{
    m_name = name;
}

QColor imageToPaint::color() const
{
    return m_color;
}

void imageToPaint::setColor(const QColor &color)
{
    m_color = color;
}


void imageToPaint::paint(QPainter *painter)
{

    painter->drawImage(boundingRect().adjusted(1, 1, -1, -1),m_original, QRect(), Qt::DiffuseAlphaDither);




}

/* slooooooootttttttttttttttttttttttttttttt */
void imageToPaint::loadStat(const bool stat)
{
    if(!stat)
    {
        qDebug()<<"error when loading texture";
        exit(0);
    }
    m_textureLoaded = true;
}

void imageToPaint::removePainting(int wallNumber)
{
    wallsTab[wallNumber].removePainting(m_original, m_copy);
    update();
}


