#ifndef WALLS_H
#define WALLS_H


#include "QString"
#include "QImage"
#include "QFile"
#include "QTextStream"
#include "QDebug"
#include "optimized.h"
#include "threadcoloration.h"
#define threadNumber 8

class Walls : public QImage
{
public:
    Walls();
    void fillInImage(QImage &img, QImage &copy, const QColor &paintColor);
    bool fillTexure(QImage &img, QImage &texture, QImage &copy);
    void removePainting(QImage &img, QImage &copy);
    void loadData (const QString &path, int wallNumber);
    bool inWall(int x, int y);

    bool m_exists;

private:
    int m_x;
    int m_y;
    int m_rightLimit;
    int m_bottomLimit;
    float m_transformation[11];
    ThreadColoration m_parallelProcess[threadNumber];
    bool m_threadStats[threadNumber];

    void getAB(const QColor &colorTopaint, float &a, float &b);
    void getL(const QColor &imageColor, float &l);
    QColor rgbFromlab(float const &l, float const &a, float const &b);


};

#endif // WALLS_H
