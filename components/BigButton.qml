import QtQuick 2.11
import QtQuick.Controls 2.4

Button {
    id: button
    property int bWidth : parent.width
    property int bHeight : parent.height/4
    property string path
    property string buttonLabel : ""
    property color bColor: "#ffffff"

    background: Rectangle {

        Image {
            id: buttonImage
            anchors.left: parent.left
            anchors.leftMargin: 15
            anchors.verticalCenter: parent.verticalCenter
            width: bHeight*3/5
            height: bHeight*3/5
            source: path
        }

        Text{
            anchors.leftMargin: 15
            height: bHeight
            font.family: "Arial"
            font.pointSize: 16
            text : qsTr(buttonLabel)
            verticalAlignment: Text.AlignVCenter
            anchors.left: buttonImage.right
            color: button.down?  "white":"#5d5b59"
        }
        color: button.down? "#a3d2e9":bColor

        implicitWidth: bWidth
        implicitHeight: bHeight




    }
}
