import QtQuick 2.4
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0

Item{
    property  ListModel bAll: all
    property  ListModel bLivingRooms: livingRooms
    property  ListModel bRooms: rooms
    property  ListModel bKitchens: kitchens
    property  ListModel bFacades: facades
    property  ListModel bBathrooms: bathrooms
    property Component bImageButtonDelegate: imageDelegate

    ListModel {
                 id:all
                 ListElement {path:"qrc:/images/kitchen1-mini.jpeg"}
                 ListElement {path:"qrc:/images/kitchen2-mini.jpeg"}
                 ListElement {path:"qrc:/images/kitchen3-mini.jpeg"}
                 ListElement {path:"qrc:/images/kitchen4-mini.jpeg"}
                 ListElement {path:"qrc:/images/livingRoom1-mini.jpeg"}
                 ListElement {path:"qrc:/images/livingRoom2-mini.jpeg"}
                 ListElement {path:"qrc:/images/livingRoom3-mini.jpeg"}
                 ListElement {path:"qrc:/images/livingRoom5-mini.jpeg"}
                 ListElement {path:"qrc:/images/room1-mini.jpeg"}
                 ListElement {path:"qrc:/images/room2-mini.jpeg"}
                 ListElement {path:"qrc:/images/room3-mini.jpeg"}
                 ListElement {path:"qrc:/images/room4-mini.jpeg"}
                 ListElement {path:"qrc:/images/room5-mini.jpeg"}
                 ListElement {path:"qrc:/images/facade1-mini.jpeg"}
                 ListElement {path:"qrc:/images/facade2-mini.jpeg"}
                 ListElement {path:"qrc:/images/facade3-mini.jpeg"}
                 ListElement {path:"qrc:/images/facade4-mini.jpeg"}
                 ListElement {path:"qrc:/images/bathroom1-mini.jpeg"}
                 ListElement {path:"qrc:/images/bathroom2-mini.jpeg"}
                 ListElement {path:"qrc:/images/bathroom3-mini.jpeg"}
    }

    ListModel {
                 id: livingRooms
                 ListElement {path:"qrc:/images/livingRoom1-mini.jpeg"}
                 ListElement {path:"qrc:/images/livingRoom2-mini.jpeg"}
                 ListElement {path:"qrc:/images/livingRoom3-mini.jpeg"}
                 ListElement {path:"qrc:/images/livingRoom5-mini.jpeg"}
    }

    ListModel {
                 id: rooms
                 ListElement {path:"qrc:/images/room1-mini.jpeg"}
                 ListElement {path:"qrc:/images/room2-mini.jpeg"}
                 ListElement {path:"qrc:/images/room3-mini.jpeg"}
                 ListElement {path:"qrc:/images/room4-mini.jpeg"}
                 ListElement {path:"qrc:/images/room5-mini.jpeg"}
    }

    ListModel {
                 id: kitchens

                 ListElement {path:"qrc:/images/kitchen1-mini.jpeg"}
                 ListElement {path:"qrc:/images/kitchen2-mini.jpeg"}
                 ListElement {path:"qrc:/images/kitchen3-mini.jpeg"}
                 ListElement {path:"qrc:/images/kitchen4-mini.jpeg"}

    }

    ListModel {
                 id: facades
                 ListElement {path:"qrc:/images/facade1-mini.jpeg"}
                 ListElement {path:"qrc:/images/facade2-mini.jpeg"}
                 ListElement {path:"qrc:/images/facade3-mini.jpeg"}
                 ListElement {path:"qrc:/images/facade4-mini.jpeg"}
    }

    ListModel {
                 id: bathrooms
                 ListElement {path:"qrc:/images/bathroom1-mini.jpeg"}
                 ListElement {path:"qrc:/images/bathroom2-mini.jpeg"}
                 ListElement {path:"qrc:/images/bathroom3-mini.jpeg"}
    }

    Component{
        id: imageDelegate
        ItemDelegate {
              id: delegateButton
              width: sidebarGrid.cellWidth
              height: sidebarGrid.cellHeight

              background: Image{
                 id:img
                 source: model.path
                 anchors.centerIn: parent
                 width: sidebarGrid.cellWidth - 10
                 height: sidebarGrid.cellHeight - 10

                 Rectangle{
                     anchors.fill: parent
                     color: delegateButton.down ? "#80ffffff" : "transparent"
                     border.width: 2
                     border.color: "white"

                 }


             }
             DropShadow {
                anchors.fill:img
                horizontalOffset: 3
                verticalOffset: 3
                radius: 8.0
                samples: 17
                color: "#80000000"
                source: img
                }


             onClicked:{
                 var str = model.path
                 str = str.replace("-mini.jpeg", "")
                 str = str.replace("qrc", "")


                 drawItem.width = img.width
                 drawItem.height = img.height
                 drawItem.x = img.x + delegateButton.x
                 drawItem.y = img.y+ delegateButton.y + sidebarGrid.y - sidebarGrid.contentY



                 drawItem.oldWidth = img.width
                 drawItem.oldHeight = img.height
                 drawItem.oldX = img.x + delegateButton.x
                 drawItem.oldY = img.y + delegateButton.y +sidebarGrid.y - sidebarGrid.contentY

                 drawItem.path = str
                 drawItem.update()
                 drawItem.visible = true
                 drawItem.enabled = false
                 sidebarGrid.interactive = false
                 sidebarGrid.enabled = false
                 fillAnimation.running = true

             }

         }
    }
}
