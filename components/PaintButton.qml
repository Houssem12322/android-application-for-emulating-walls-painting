import QtQuick 2.11
import QtQuick.Controls 2.4

Button {
    id: button
    property color bColor: "#aaFF69B4"
    property int type: 1
    property color wColor: "transparent"
    property string wPath: ""
    property string wReference: ""
    property int wWallNumber
    z:11
    height: 20
    width: 20
    visible: false
    MouseArea{
        x: - 30
        y: - 30

        width: parent.width + 60
        height: parent.height + 60
        onClicked:{
            button.clicked()
        }
    }

    background: Rectangle {
         color: button.down? "#20FF69B4":bColor
         border.width: 2
         border.color: "#505d5b59"
         radius: 250
        }

}
