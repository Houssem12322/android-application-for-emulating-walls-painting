import QtQuick 2.11
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.11

Rectangle{

     id:parentRect
     property int type: 0
     property int bIndex
     property color bWallColor: "transparent"
     property string bReference: ""
     property string bPath: ""
     property int bWallNumber
     signal colorClicked()

     z:12
     color: "#60ffffff"
     radius: 10
     visible: false
     LinearGradient {
         anchors.fill: parent
         start: Qt.point(0, 0)
         end: Qt.point(parent.width, parent.height)
         gradient: Gradient {
             GradientStop { position: 0.0; color: "#60ffffff" }
             GradientStop { position: 1.0; color: "#60eaeaea" }
         }
         source: parent
         }

     ColumnLayout{
         spacing: 2
         anchors.centerIn: parent
         Rectangle{
             Layout.alignment: Qt.AlignCenter
             width: parentRect.width - 20
             height: parentRect.width - 20
             radius: 200
             color: bWallColor
             border.color: "#eeeeee"
             border.width: 1
             clip: true


             Image{
                 id: source
                 fillMode: Image.PreserveAspectCrop
                 anchors.centerIn: parent
                 source: bPath + "-mini.png"
                 visible: false
             }

             Rectangle{
                 id:mask
                 anchors.fill: parent
                 radius: 200
                 visible: false
             }

             OpacityMask {
                     anchors.fill: mask
                     source: source
                     maskSource: mask
                 }


             MouseArea{
                 id: click
                 anchors.fill: parent
                 onClicked: {
                    parentRect.colorClicked()
                 }
             }

             Rectangle{
                 id: recWhenPressed
                 width: parent.width
                 height: parent.width
                 radius: 250
                 color: click.pressed ? "#601e1b18":"transparent"

             }

          }


         Text{
             Layout.alignment: Qt.AlignCenter
             font.family: "Cordia New"
             font.pointSize: 8
             text: bReference

        }
     }
}


