import QtQml 2.11

QtObject{
    property variant now: { "forMore": "Pour plus\n d'informations visitez notre\n site web", "visitB": "Visiter",
                            "paintB": "Peindre des murs", "videoB": "Explorez nos videos", "catalogB": "Catalogue",
                            "contactB": "Contactez nous", "home": "Accueil", "painting": "peindre",
                            "videos": "Videos", "catalog": "Catalogue", "contact": "Contacts", "adress": "Adresse:",
                            "e-mail": "E-mail:", "phone": "Mobile:", "website": "SiteWeb:", "colors": "Peinture",
                            "back": "Retour", "all":"Tous", "lounges": "Salons", "rooms": "Chambres", "kitchens": "Cuisines",
                            "facades": "Façades", "bathrooms": "Toilettes", "ral": "Ral",
                            "jungle": "Jingle", "decorative": "Décorative", "crocodile": "Crocodile",
                            "bark": "Ecorce", "elephant": "Eléphant", "giraffe": "girafe", "leopard": "Léopard",
                            "tiger": "Tigre", "tiger-B": "Tigre-B", "zebra": "Zèbre", "python": "python",
                            "settings": "Options", "about": "A propos", "languages": "Langues", "cancel": "Annuler",
                            "ok": "OK", "lang": "fr", "adrInfo":"Cité Tamachite, en face de la gare\n routière, Batna, Algerie",
    "aboutContent":"Cette application a été développée par la boite de développement SCRIPTIHA. Elle a pour but de simuler le processus de peinture des murs avec des peintures simples ou des textures. Pour plus d’informations veuillez contacter la boite sur : +213(0)770114768 ou +213(0)698552619"}

    property variant fr: { "forMore": "Pour plus\n d'informations visitez notre\n site web", "visitB": "Visiter",
                           "paintB": "Peindre des murs", "videoB": "Explorez nos videos", "catalogB": "Catalogue",
                           "contactB": "Contactez nous", "home": "Accueil", "painting": "peindre",
                           "videos": "Videos", "catalog": "Catalogue", "contact": "Contacts", "adress": "Adresse:",
                           "e-mail": "E-mail:", "phone": "Mobile:", "website": "SiteWeb:", "colors": "Peinture",
                           "back": "Retour", "all":"Tous", "lounges": "Salons", "rooms": "Chambres", "kitchens": "Cuisines",
                           "facades": "Façades", "bathrooms": "Toilettes", "ral": "Ral",
                           "jungle": "Jingle", "decorative": "Décorative", "crocodile": "Crocodile",
                           "bark": "Ecorce", "elephant": "Eléphant", "giraffe": "girafe", "leopard": "Léopard",
                           "tiger": "Tigre", "tiger-B": "Tigre-B", "zebra": "Zèbre", "python": "python",
                           "settings": "Options", "about": "A propos", "languages": "Langues", "cancel": "Annuler",
                           "ok": "OK", "lang": "fr", "adrInfo":"Cité Tamachite, en face de la gare\n routière, Batna, Algerie",
    "aboutContent":"Cette application a été développée par la boite de développement SCRIPTIHA. Elle a pour but de simuler le processus de peinture des murs avec des peintures simples ou des textures. Pour plus d’informations veuillez contacter la boite sur : +213(0)770114768 ou +213(0)698552619"}

    property variant en: { "forMore": "For more\n informations consult\n our website", "visitB": "Visit",
                           "paintB": "Painting walls", "videoB": "Explore our videos", "catalogB": "Catalog",
                           "contactB": "Contact us", "home": "Home", "painting": "Painting",
                           "videos": "Videos", "catalog": "Catalog", "contact": "Contacts", "adress": "Adress:",
                           "e-mail": "E-mail:", "phone": "Phone:", "website": "Website:", "colors": "Paint",
                           "back": "Back", "all":"All", "lounges": "Lounges", "rooms": "Rooms", "kitchens": "Kitchens",
                           "facades": "Facades", "bathrooms": "Bathrooms", "ral": "Ral",
                           "jungle": "Jungle", "decorative": "Decorative", "crocodile": "Crocodile",
                           "bark": "Bark", "elephant": "Elephant", "giraffe": "giraffe", "leopard": "Leopard",
                           "tiger": "Tiger", "tiger-B": "Tiger-B", "zebra": "Zebra", "python": "python",
                           "settings": "Settings", "about": "About", "languages": "Languages", "cancel": "Cancel",
                           "ok": "OK", "lang": "en", "adrInfo":"Tamachite City, In Front of the Bus\n Station, Batna, Algeria",
    "aboutContent":"This application was developed by the SCRIPTIHA group. It aims to simulate the process of painting walls with simple paints or textures. For more information please contact the us on: +213(0)770114768 or +213(0)698552619"}

    property variant ar: { "forMore": "زوروا موقعنا\n لمزيد من المعلومات", "visitB": "دخول",
                           "paintB": "دهن الغرف", "videoB": "مشاهدة فيديوهاتنا", "catalogB": "قائمة منتجاتنا",
                           "contactB": "إتصلوا بنا", "home": "إستقبال", "painting": "طلاء",
                           "videos": "فيديوهات", "catalog": "منتجاتنا", "contact": "إتصلوا بنا", "adress": "العنوان:",
                           "e-mail": "البريد:", "phone": "الهاتف:", "website": "الموقع:", "colors": "الطلاء",
                           "back": "عودة", "all":"كل الغرف", "lounges": "الصالات", "rooms": "الغرف", "kitchens": "المطابخ",
                           "facades": "الواجهات", "bathrooms": "الحمامات", "ral": "Ral",
                           "jungle": "أدغال", "decorative": "ديكوري", "crocodile": "التمساح",
                           "bark": "اللحاء", "elephant": "الفيل", "giraffe": "الزرافة", "leopard": "الفهد",
                           "tiger": "النمر", "tiger-B": "النمر-أ", "zebra": "حمار الوحش", "python": "الثعبان",
                           "settings": "إعدادات", "about": "معلومات", "languages": "اللغات", "cancel": "إلغاء",
                           "ok": "موافق", "lang": "ar", "adrInfo":"حي تامشيط، مقابل محطة الحافلات،\n باتنة، الجزائر",
    "aboutContent":"تم تطوير هذا التطبيق من قبل مجموعة SCRIPTIHA. ويهدف إلى محاكاة عملية طلاء الجدران بطلاء بسيط أو زخارف. لمزيد من المعلومات ، يرجى الاتصال بالأرقام التالية: 770114768(0)213+ أو 698552619(0)213+ "}

}
