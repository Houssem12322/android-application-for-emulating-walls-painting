import QtQuick 2.4
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0

Item{
    property  ListModel bRal: ral
    property  ListModel bJungle: jungle
    property  ListModel bDecor: decor
    property Component bColorsChoicesItem: colorElement

    ListModel {
                 id:ral
                 ListElement{color:"#BEBD7F"; path:""; reference:"ral-1000"}
                 ListElement{color:"#C2B078"; path:""; reference:"ral-1001"}
                 ListElement{color:"#C6A664"; path:""; reference:"ral-1002"}
                 ListElement{color:"#E5BE01"; path:""; reference:"ral-1003"}
                 ListElement{color:"#CDA434"; path:""; reference:"ral-1004"}
                 ListElement{color:"#A98307"; path:""; reference:"ral-1005"}
                 ListElement{color:"#E4A010"; path:""; reference:"ral-1006"}
                 ListElement{color:"#DC9D00"; path:""; reference:"ral-1007"}
                 ListElement{color:"#8A6642"; path:""; reference:"ral-1011"}
                 ListElement{color:"#C7B446"; path:""; reference:"ral-1012"}
                 ListElement{color:"#EAE6CA"; path:""; reference:"ral-1013"}
                 ListElement{color:"#E1CC4F"; path:""; reference:"ral-1014"}
                 ListElement{color:"#E6D690"; path:""; reference:"ral-1015"}
                 ListElement{color:"#EDFF21"; path:""; reference:"ral-1016"}
                 ListElement{color:"#F8F32B"; path:""; reference:"ral-1017"}
                 ListElement{color:"#9E9764"; path:""; reference:"ral-1018"}
                 ListElement{color:"#999950"; path:""; reference:"ral-1019"}
                 ListElement{color:"#F3DA0B"; path:""; reference:"ral-1020"}
                 ListElement{color:"#FAD201"; path:""; reference:"ral-1021"}
                 ListElement{color:"#AEA04B"; path:""; reference:"ral-1023"}
                 ListElement{color:"#FFFF00"; path:""; reference:"ral-1024"}
                 ListElement{color:"#9D9101"; path:""; reference:"ral-1026"}
                 ListElement{color:"#F4A900"; path:""; reference:"ral-1027"}
                 ListElement{color:"#D6AE01"; path:""; reference:"ral-1028"}
                 ListElement{color:"#F3A505"; path:""; reference:"ral-1032"}
                 ListElement{color:"#EFA94A"; path:""; reference:"ral-1033"}
                 ListElement{color:"#6A5D4D"; path:""; reference:"ral-1034"}
                 ListElement{color:"#705335"; path:""; reference:"ral-1035"}
                 ListElement{color:"#F39F18"; path:""; reference:"ral-1036"}
                 ListElement{color:"#ED760E"; path:""; reference:"ral-1037"}
                 ListElement{color:"#C93C20"; path:""; reference:"ral-2000"}
                 ListElement{color:"#CB2821"; path:""; reference:"ral-2001"}
                 ListElement{color:"#FF7514"; path:""; reference:"ral-2002"}
                 ListElement{color:"#F44611"; path:""; reference:"ral-2003"}
                 ListElement{color:"#FF2301"; path:""; reference:"ral-2004"}
                 ListElement{color:"#FFA420"; path:""; reference:"ral-2005"}
                 ListElement{color:"#F75E25"; path:""; reference:"ral-2007"}
                 ListElement{color:"#F54021"; path:""; reference:"ral-2008"}
                 ListElement{color:"#D84B20"; path:""; reference:"ral-2009"}
                 ListElement{color:"#EC7C26"; path:""; reference:"ral-2010"}
                 ListElement{color:"#E55137"; path:""; reference:"ral-2011"}
                 ListElement{color:"#A52019"; path:""; reference:"ral-2012"}
                 ListElement{color:"#AF2B1E"; path:""; reference:"ral-2013"}
                 ListElement{color:"#A2231D"; path:""; reference:"ral-3000"}
                 ListElement{color:"#9B111E"; path:""; reference:"ral-3001"}
                 ListElement{color:"#75151E"; path:""; reference:"ral-3002"}
                 ListElement{color:"#5E2129"; path:""; reference:"ral-3003"}
                 ListElement{color:"#412227"; path:""; reference:"ral-3004"}
                 ListElement{color:"#642424"; path:""; reference:"ral-3005"}
                 ListElement{color:"#781F19"; path:""; reference:"ral-3007"}
                 ListElement{color:"#C1876B"; path:""; reference:"ral-3009"}
                 ListElement{color:"#A12312"; path:""; reference:"ral-3011"}
                 ListElement{color:"#D36E70"; path:""; reference:"ral-3012"}
                 ListElement{color:"#EA899A"; path:""; reference:"ral-3013"}
                 ListElement{color:"#B32821"; path:""; reference:"ral-3014"}
                 ListElement{color:"#E63244"; path:""; reference:"ral-3015"}
                 ListElement{color:"#D53032"; path:""; reference:"ral-3016"}
                 ListElement{color:"#CC0605"; path:""; reference:"ral-3017"}
                 ListElement{color:"#D95030"; path:""; reference:"ral-3018"}
                 ListElement{color:"#F80000"; path:""; reference:"ral-3020"}
                 ListElement{color:"#FE0000"; path:""; reference:"ral-3022"}
                 ListElement{color:"#C51D34"; path:""; reference:"ral-3024"}
                 ListElement{color:"#CB3234"; path:""; reference:"ral-3026"}
                 ListElement{color:"#B32428"; path:""; reference:"ral-3027"}
                 ListElement{color:"#721422"; path:""; reference:"ral-3028"}
                 ListElement{color:"#B44C43"; path:""; reference:"ral-3031"}
                 ListElement{color:"#6D3F5B"; path:""; reference:"ral-3032"}
                 ListElement{color:"#922B3E"; path:""; reference:"ral-3033"}
                 ListElement{color:"#DE4C8A"; path:""; reference:"ral-4001"}
                 ListElement{color:"#641C34"; path:""; reference:"ral-4002"}
                 ListElement{color:"#6C4675"; path:""; reference:"ral-4003"}
                 ListElement{color:"#A03472"; path:""; reference:"ral-4004"}
                 ListElement{color:"#4A192C"; path:""; reference:"ral-4005"}
                 ListElement{color:"#924E7D"; path:""; reference:"ral-4006"}
                 ListElement{color:"#A18594"; path:""; reference:"ral-4007"}
                 ListElement{color:"#CF3476"; path:""; reference:"ral-4008"}
                 ListElement{color:"#8673A1"; path:""; reference:"ral-4009"}
                 ListElement{color:"#6C6874"; path:""; reference:"ral-4010"}
                 ListElement{color:"#354D73"; path:""; reference:"ral-4011"}
                 ListElement{color:"#1F3438"; path:""; reference:"ral-4012"}
                 ListElement{color:"#20214F"; path:""; reference:"ral-5000"}
                 ListElement{color:"#1D1E33"; path:""; reference:"ral-5001"}
                 ListElement{color:"#18171C"; path:""; reference:"ral-5002"}
                 ListElement{color:"#1E2460"; path:""; reference:"ral-5003"}
                 ListElement{color:"#3E5F8A"; path:""; reference:"ral-5004"}
                 ListElement{color:"#26252D"; path:""; reference:"ral-5005"}
                 ListElement{color:"#025669"; path:""; reference:"ral-5007"}
                 ListElement{color:"#0E294B"; path:""; reference:"ral-5008"}
                 ListElement{color:"#231A24"; path:""; reference:"ral-5009"}
                 ListElement{color:"#3B83BD"; path:""; reference:"ral-5010"}
                 ListElement{color:"#1E213D"; path:""; reference:"ral-5011"}
                 ListElement{color:"#606E8C"; path:""; reference:"ral-5012"}
                 ListElement{color:"#2271B3"; path:""; reference:"ral-5013"}
                 ListElement{color:"#063971"; path:""; reference:"ral-5014"}
                 ListElement{color:"#3F888F"; path:""; reference:"ral-5015"}
                 ListElement{color:"#1B5583"; path:""; reference:"ral-5017"}
                 ListElement{color:"#1D334A"; path:""; reference:"ral-5018"}
                 ListElement{color:"#256D7B"; path:""; reference:"ral-5019"}
                 ListElement{color:"#252850"; path:""; reference:"ral-5020"}
                 ListElement{color:"#49678D"; path:""; reference:"ral-5021"}
                 ListElement{color:"#5D9B9B"; path:""; reference:"ral-5022"}
                 ListElement{color:"#2A6478"; path:""; reference:"ral-5023"}
                 ListElement{color:"#102C54"; path:""; reference:"ral-5024"}
                 ListElement{color:"#316650"; path:""; reference:"ral-5025"}
                 ListElement{color:"#287233"; path:""; reference:"ral-5026"}
                 ListElement{color:"#2D572C"; path:""; reference:"ral-6000"}
                 ListElement{color:"#424632"; path:""; reference:"ral-6001"}
                 ListElement{color:"#1F3A3D"; path:""; reference:"ral-6002"}
                 ListElement{color:"#2F4538"; path:""; reference:"ral-6003"}
                 ListElement{color:"#3E3B32"; path:""; reference:"ral-6004"}
                 ListElement{color:"#343B29"; path:""; reference:"ral-6005"}
                 ListElement{color:"#39352A"; path:""; reference:"ral-6006"}
                 ListElement{color:"#31372B"; path:""; reference:"ral-6007"}
                 ListElement{color:"#35682D"; path:""; reference:"ral-6008"}
                 ListElement{color:"#587246"; path:""; reference:"ral-6009"}
                 ListElement{color:"#343E40"; path:""; reference:"ral-6010"}
                 ListElement{color:"#6C7156"; path:""; reference:"ral-6011"}
                 ListElement{color:"#47402E"; path:""; reference:"ral-6012"}
                 ListElement{color:"#3B3C36"; path:""; reference:"ral-6013"}
                 ListElement{color:"#1E5945"; path:""; reference:"ral-6014"}
                 ListElement{color:"#4C9141"; path:""; reference:"ral-6015"}
                 ListElement{color:"#57A639"; path:""; reference:"ral-6016"}
                 ListElement{color:"#BDECB6"; path:""; reference:"ral-6017"}
                 ListElement{color:"#2E3A23"; path:""; reference:"ral-6018"}
                 ListElement{color:"#89AC76"; path:""; reference:"ral-6019"}
                 ListElement{color:"#25221B"; path:""; reference:"ral-6020"}
                 ListElement{color:"#308446"; path:""; reference:"ral-6021"}
                 ListElement{color:"#3D642D"; path:""; reference:"ral-6022"}
                 ListElement{color:"#015D52"; path:""; reference:"ral-6024"}
                 ListElement{color:"#84C3BE"; path:""; reference:"ral-6025"}
                 ListElement{color:"#2C5545"; path:""; reference:"ral-6026"}
                 ListElement{color:"#20603D"; path:""; reference:"ral-6027"}
                 ListElement{color:"#317F43"; path:""; reference:"ral-6028"}
                 ListElement{color:"#497E76"; path:""; reference:"ral-6029"}
                 ListElement{color:"#7FB5B5"; path:""; reference:"ral-6032"}
                 ListElement{color:"#1C542D"; path:""; reference:"ral-6035"}
                 ListElement{color:"#193737"; path:""; reference:"ral-6036"}
                 ListElement{color:"#008F39"; path:""; reference:"ral-6037"}
                 ListElement{color:"#00BB2D"; path:""; reference:"ral-6038"}
                 ListElement{color:"#78858B"; path:""; reference:"ral-7000"}
                 ListElement{color:"#8A9597"; path:""; reference:"ral-7001"}
                 ListElement{color:"#7E7B52"; path:""; reference:"ral-7002"}
                 ListElement{color:"#6C7059"; path:""; reference:"ral-7003"}
                 ListElement{color:"#969992"; path:""; reference:"ral-7004"}
                 ListElement{color:"#646B63"; path:""; reference:"ral-7005"}
                 ListElement{color:"#6D6552"; path:""; reference:"ral-7006"}
                 ListElement{color:"#6A5F31"; path:""; reference:"ral-7008"}
                 ListElement{color:"#4D5645"; path:""; reference:"ral-7009"}
                 ListElement{color:"#4C514A"; path:""; reference:"ral-7010"}
                 ListElement{color:"#434B4D"; path:""; reference:"ral-7011"}
                 ListElement{color:"#4E5754"; path:""; reference:"ral-7012"}
                 ListElement{color:"#464531"; path:""; reference:"ral-7013"}
                 ListElement{color:"#434750"; path:""; reference:"ral-7015"}
                 ListElement{color:"#293133"; path:""; reference:"ral-7016"}
                 ListElement{color:"#23282B"; path:""; reference:"ral-7021"}
                 ListElement{color:"#332F2C"; path:""; reference:"ral-7022"}
                 ListElement{color:"#686C5E"; path:""; reference:"ral-7023"}
                 ListElement{color:"#474A51"; path:""; reference:"ral-7024"}
                 ListElement{color:"#2F353B"; path:""; reference:"ral-7026"}
                 ListElement{color:"#8B8C7A"; path:""; reference:"ral-7030"}
                 ListElement{color:"#474B4E"; path:""; reference:"ral-7031"}
                 ListElement{color:"#B8B799"; path:""; reference:"ral-7032"}
                 ListElement{color:"#7D8471"; path:""; reference:"ral-7033"}
                 ListElement{color:"#8F8B66"; path:""; reference:"ral-7034"}
                 ListElement{color:"#D7D7D7"; path:""; reference:"ral-7035"}
                 ListElement{color:"#7F7679"; path:""; reference:"ral-7036"}
                 ListElement{color:"#7D7F7D"; path:""; reference:"ral-7037"}
                 ListElement{color:"#B5B8B1"; path:""; reference:"ral-7038"}
                 ListElement{color:"#6C6960"; path:""; reference:"ral-7039"}
                 ListElement{color:"#9DA1AA"; path:""; reference:"ral-7040"}
                 ListElement{color:"#8D948D"; path:""; reference:"ral-7042"}
                 ListElement{color:"#4E5452"; path:""; reference:"ral-7043"}
                 ListElement{color:"#CAC4B0"; path:""; reference:"ral-7044"}
                 ListElement{color:"#909090"; path:""; reference:"ral-7045"}
                 ListElement{color:"#82898F"; path:""; reference:"ral-7046"}
                 ListElement{color:"#D0D0D0"; path:""; reference:"ral-7047"}
                 ListElement{color:"#898176"; path:""; reference:"ral-7048"}
                 ListElement{color:"#826C34"; path:""; reference:"ral-8000"}
                 ListElement{color:"#955F20"; path:""; reference:"ral-8001"}
                 ListElement{color:"#6C3B2A"; path:""; reference:"ral-8002"}
                 ListElement{color:"#734222"; path:""; reference:"ral-8003"}
                 ListElement{color:"#8E402A"; path:""; reference:"ral-8004"}
                 ListElement{color:"#59351F"; path:""; reference:"ral-8007"}
                 ListElement{color:"#6F4F28"; path:""; reference:"ral-8008"}
                 ListElement{color:"#5B3A29"; path:""; reference:"ral-8011"}
                 ListElement{color:"#592321"; path:""; reference:"ral-8012"}
                 ListElement{color:"#382C1E"; path:""; reference:"ral-8014"}
                 ListElement{color:"#633A34"; path:""; reference:"ral-8015"}
                 ListElement{color:"#4C2F27"; path:""; reference:"ral-8016"}
                 ListElement{color:"#45322E"; path:""; reference:"ral-8017"}
                 ListElement{color:"#403A3A"; path:""; reference:"ral-8019"}
                 ListElement{color:"#212121"; path:""; reference:"ral-8022"}
                 ListElement{color:"#A65E2E"; path:""; reference:"ral-8023"}
                 ListElement{color:"#79553D"; path:""; reference:"ral-8024"}
                 ListElement{color:"#755C48"; path:""; reference:"ral-8025"}
                 ListElement{color:"#4E3B31"; path:""; reference:"ral-8028"}
                 ListElement{color:"#763C28"; path:""; reference:"ral-8029"}
                 ListElement{color:"#FDF4E3"; path:""; reference:"ral-9001"}
                 ListElement{color:"#E7EBDA"; path:""; reference:"ral-9002"}
                 ListElement{color:"#F4F4F4"; path:""; reference:"ral-9003"}
                 ListElement{color:"#282828"; path:""; reference:"ral-9004"}
                 ListElement{color:"#0A0A0A"; path:""; reference:"ral-9005"}
                 ListElement{color:"#A5A5A5"; path:""; reference:"ral-9006"}
                 ListElement{color:"#8F8F8F"; path:""; reference:"ral-9007"}
                 ListElement{color:"#FFFFFF"; path:""; reference:"ral-9010"}
                 ListElement{color:"#1C1C1C"; path:""; reference:"ral-9011"}
                 ListElement{color:"#F6F6F6"; path:""; reference:"ral-9016"}
                 ListElement{color:"#1E1E1E"; path:""; reference:"ral-9017"}
                 ListElement{color:"#D7D7D7"; path:""; reference:"ral-9018"}
                 ListElement{color:"#9C9C9C"; path:""; reference:"ral-9022"}
                 ListElement{color:"#828282"; path:""; reference:"ral-9023"}

    }

    ListModel {
                 id: jungle
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/crocodile"; reference:"crocodile"; isJungle: true}
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/ecorce"; reference:"bark"; isJungle: true}
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/elephant"; reference:"elephant"; isJungle: true}
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/girafe"; reference:"giraffe"; isJungle: true}
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/leopard"; reference:"leopard"; isJungle: true}
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/tigre"; reference:"tiger"; isJungle: true}
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/tigreBlanc"; reference:"tiger-B"; isJungle: true}
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/zebre"; reference:"zebra"; isJungle: true}
                 ListElement{color:"transparent"; path:"qrc:/images/jungle/python"; reference:"python"; isJungle: true}
    }

    ListModel {
                 id:decor                
                 ListElement{color:"transparent"; path:"qrc:/images/decorative/DSC_0001"; reference:"DSC_0001"}
                 ListElement{color:"transparent"; path:"qrc:/images/decorative/DSC_0009"; reference:"DSC_0009"}
                 ListElement{color:"transparent"; path:"qrc:/images/decorative/DSC_0012"; reference:"DSC_0012"}
                 ListElement{color:"transparent"; path:"qrc:/images/decorative/DSC_0062"; reference:"DSC_0062"}
                 ListElement{color:"transparent"; path:"qrc:/images/decorative/DSC_0066"; reference:"DSC_0066"}
                 ListElement{color:"transparent"; path:"qrc:/images/decorative/DSC_0046"; reference:"DSC_0046"}
                 ListElement{color:"transparent"; path:"qrc:/images/decorative/DSC_0625"; reference:"DSC_0625"}
    }


    Component{
        id: colorElement
        Item {

        id: colorsChoicesItem
        width: colorsGrids.cellWidth
        height: colorsGrids.cellHeight

        Column{
            width: colorsGrids.cellWidth - 10
            height: colorsGrids.cellHeight -10
            anchors.centerIn: parent
            ItemDelegate{
            id: colorsChoices
            width: parent.width
            height: parent.width

            anchors.horizontalCenter: parent.horizontalCenter

            background: Rectangle{
                width: colorsChoices.width
                height: colorsChoices.width
                radius: 200
                color: model.color
                border.color: "#eeeeee"
                border.width: 1
                clip: true

                Image{
                    id: source
                    fillMode: Image.PreserveAspectCrop
                    anchors.centerIn: parent
                    source: model.path + "-mini.png"
                    visible: false
                }

                Rectangle{
                    id:mask
                    anchors.fill: parent
                    radius: 200
                    visible: false
                }

                OpacityMask {
                        anchors.fill: mask
                        source: source
                        maskSource: mask
                    }



                Rectangle{
                    id: recWhenPressed
                    width: parent.width
                    height: parent.width
                    radius: 200
                    color: colorsChoices.pressed ? "#601e1b18":"transparent"

                }

             }

            onClicked: {
                if(timer.running)
                {
                    var i=0;
                    var exist = false;
                    for(i=0; i<choosenColorsModel.count; i++)
                    {
                        if(choosenColorsModel.get(i).reference === model.reference)
                        {
                            exist = true;
                            break;
                        }
                    }

                    if(!exist)
                    {
                        choosenColorsModel.append({"color":model.color, "path": model.path, "reference": model.reference})
                        toolTipAnimation.stop()
                        toolTip.x = colorsChoicesItem.x + colorsDialog.x + colorsGrids.x - (toolTip.width - colorsChoicesItem.width)/2
                        toolTip.y = colorsChoicesItem.y + colorsDialog.x +  colorsGrids.y - colorsGrids.contentY -5
                        toolTip.opacity = 1.0
                        toolTip.visible = true

                        toolTipAnimation.running = true
                    }
                    timer.stop()
                }else
                    timer.restart()

            }

        }
        Text{            
            anchors.horizontalCenter: parent.horizontalCenter
            font.family: "Cordia New"
            font.pointSize: 8
            text:model.isJungle ? langue.now[model.reference]:model.reference

        }
        }
   }
}

}
