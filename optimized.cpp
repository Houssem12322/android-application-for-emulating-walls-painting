#include "optimized.h"

Optimized::Optimized(QObject *parent) : QObject(parent)
{

}

void Optimized::LoadImage(QImage *img, QString path)
{
    if(!img->load(path + ".jpeg", "JPG"))
    {
        emit finish(false);
    }
    else
    {
        emit finish(true);
    }
}

