TEMPLATE = app
TARGET = paintureV0
QT += qml quick quickcontrols2 webview

SOURCES += \
    main.cpp \
    imagetopaint.cpp \
    walls.cpp \
    optimized.cpp \
    threadcoloration.cpp

RESOURCES += \
    main.qml \
    qtquickcontrols2.conf \
    icons/gallery/index.theme \
    $$files(icons/*.png, true) \
    $$files(videos/*.gif) \
    $$files(components/*.qml) \
    $$files(pages/*.qml)



qnx:target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin

android {
    SAMPLES_INSTALL_PATH=/assets/images
} else {

    SAMPLES_INSTALL_PATH=$$OUT_PWD/images
}

# - setup the 'make install' step
samples.path = $$SAMPLES_INSTALL_PATH
samples.files += images.rcc
samples.depends += FORCE

INSTALLS += samples
!isEmpty(target.path): INSTALLS += target


DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat
HEADERS += \
    imagetopaint.h \
    walls.h \
    optimized.h \
    threadcoloration.h

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
