#include "threadcoloration.h"
#include "QDebug" // to remove

ThreadColoration::ThreadColoration()
{
    m_oneDivthree = 1.0/3.0;
    m_gama = 16.0/116.0;
    m_100D255 = 100.0/255.0;
}


void ThreadColoration::set(QImage *tImg, QImage *tWall, QImage *tCopy, QColor tPaintColor, int tY, int m_tY, int m_tX, int tLinePerThread, int tThreadNumber, bool *tThreadStat)
{
    img = tImg;
    wall = tWall;
    copy = tCopy;
    paintColor = tPaintColor;
    y = tY;
    m_y = m_tY;
    m_x = m_tX;
    linePerThread = tLinePerThread;
    threadNumber = tThreadNumber;
    threadStat = tThreadStat;
    colorationOrTexture = 1;
}

void ThreadColoration::setTexture(QImage *tImg, QImage *tWall, QImage *tTexture, QImage *tCopy, int tY, int m_tY, int m_tX, int tLinePerThread, int tThreadNumber, bool *tThreadStat, float *tTransformation)
{
    img = tImg;
    wall = tWall;
    copy = tCopy;
    texture = tTexture;
    y = tY;
    m_y = m_tY;
    m_x = m_tX;
    linePerThread = tLinePerThread;
    threadNumber = tThreadNumber;
    threadStat = tThreadStat;
    transformation = tTransformation;
    colorationOrTexture = 0;
}

void ThreadColoration::setToRemovePainting(QImage *tImg, QImage *tWall, QImage *tCopy, int tY, int m_tY, int m_tX, int tLinePerThread, int tThreadNumber, bool *tThreadStat)
{
    img = tImg;
    wall = tWall;
    copy = tCopy;
    y = tY;
    m_y = m_tY;
    m_x = m_tX;
    linePerThread = tLinePerThread;
    threadNumber = tThreadNumber;
    threadStat = tThreadStat;
    colorationOrTexture = 2;
}

void ThreadColoration::run()
{
    if(colorationOrTexture == 1){
        float colorValue = paintColor.valueF();
        int height = wall->height();
        int width = wall->width();
        for(int j=y, k= 0; j<height && k< linePerThread; j++, k++)
        {

            QRgb* globalImage = (QRgb*)img->scanLine(j+m_y);
            QRgb* originalPixel = (QRgb*)copy->scanLine(j+m_y);
            QRgb* wallImage = (QRgb*)wall->scanLine(j);
            int globalIndex = m_x ;
            for(int i=0; i<width; i++)
            {
                float transparency = (float)qAlpha(wallImage[i]) /255.0;
                if( transparency > 0.1)
                {

                    if(colorValue > 0.70)
                    {

                         QColor tmp = QColor().fromHsvF(paintColor.hsvHueF(), paintColor.hsvSaturationF(), (QColor(wallImage[i]).valueF() + 2.0*colorValue)/3.0);
                         if(transparency < 1.0)
                         {

                             tmp.setAlpha(255);
                             tmp.setRed((int)(tmp.red()*transparency +  (float)qRed(globalImage[globalIndex]) * (1.0- transparency)));
                             tmp.setBlue((int)(tmp.blue()*transparency  +  (float)qBlue(globalImage[globalIndex]) * (1.0- transparency)));
                             tmp.setGreen((int)(tmp.green()*transparency  +  (float)qGreen(globalImage[globalIndex]) * (1.0- transparency)));


                         }


                         globalImage[globalIndex] = tmp.rgba();
                    }else
                    {
                        float A, B, LC, LW;
                        getAB(paintColor, A, B);
                        getL(paintColor, LC);
                        getL(QColor(wallImage[i]), LW);

                        QColor tmp = rgbFromlab((LC*3+LW)/4, A, B);


                        if(transparency < 1.0 )
                        {
                            tmp.setAlpha(255);
                            tmp.setRed((int)(tmp.red()*transparency  +  qRed(originalPixel[globalIndex]) * (1.0- transparency)));
                            tmp.setBlue((int)(tmp.blue()*transparency  +  qBlue(originalPixel[globalIndex]) * (1.0- transparency)));
                            tmp.setGreen((int)(tmp.green()*transparency  +  qGreen(originalPixel[globalIndex]) * (1.0- transparency)));

                        }

                        globalImage[globalIndex] = tmp.rgba();
                    }


                }
                globalIndex +=1;

            }

        }

    }
    else if(colorationOrTexture == 0)
    {

        int height = wall->height();
        int width = wall->width();
        for(int j=y, k= 0; j<height && k< linePerThread; j++, k++)
        {

            QRgb* globalImage = (QRgb*)img->scanLine(j+m_y);
            QRgb* originalPixel = (QRgb*)copy->scanLine(j+m_y);
            QRgb* textureImage = (QRgb*)texture->scanLine(j+transformation[10]);
            QRgb* wallImage = (QRgb*)wall->scanLine(j);
            int globalIndex = m_x ;
            int textureIndex = transformation[9];
            for(int i=0; i<width; i+=1)
            {

                float transparency = qAlpha(wallImage[i]) /255.0;
                if(qAlpha(wallImage[i]) > 0)
                {
                    QColor tmpTextureColor(textureImage[textureIndex]);

                    float colorValue = tmpTextureColor.valueF();
                    if(colorValue > 0.70)
                    {

                        QColor tmp = QColor().fromHsvF(tmpTextureColor.hsvHueF(), tmpTextureColor.hsvSaturationF(), (QColor(wallImage[i]).valueF() + 3*colorValue)/4.0).rgba();
                        if(transparency < 1.0)
                        {

                            tmp.setAlpha(255);
                            tmp.setRed((int)(tmp.red()*transparency +  qRed(originalPixel[globalIndex]) * (1.0- transparency)));
                            tmp.setBlue((int)(tmp.blue()*transparency  +  qBlue(originalPixel[globalIndex]) * (1.0- transparency)));
                            tmp.setGreen((int)(tmp.green()*transparency  +  qGreen(originalPixel[globalIndex]) * (1.0- transparency)));
                        }
                        globalImage[globalIndex] = tmp.rgba();

                    }else
                    {
                        float A, B, LC, LW;
                        getAB(tmpTextureColor, A, B);
                        getL(tmpTextureColor, LC);
                        getL(QColor(wallImage[i]), LW);

                        QColor tmp = rgbFromlab((LC*3+LW)/4.0, A, B);
                        if(transparency < 1.0)
                        {

                            tmp.setAlpha(255);
                            tmp.setRed((int)(tmp.red()*transparency +  qRed(originalPixel[globalIndex]) * (1.0- transparency)));
                            tmp.setBlue((int)(tmp.blue()*transparency  +  qBlue(originalPixel[globalIndex]) * (1.0- transparency)));
                            tmp.setGreen((int)(tmp.green()*transparency  +  qGreen(originalPixel[globalIndex]) * (1.0- transparency)));
                        }
                        globalImage[globalIndex] = tmp.rgba();
                    }



                }
                globalIndex +=1;
                textureIndex +=1;

            }

        }
    } else
    {
        int height = wall->height();
        int width = wall->width();
        for(int j=y, k= 0; j<height && k< linePerThread; j++, k++)
        {

            QRgb* globalImage = (QRgb*)img->scanLine(j+m_y);
            QRgb* wallImage = (QRgb*)wall->scanLine(j);
            QRgb* originalPixel = (QRgb*)copy->scanLine(j+m_y);
            int globalIndex = m_x ;
            for(int i=0; i<width; i+=1)
            {
                if(qAlpha(wallImage[i]) > 0 )
                {

                      globalImage[globalIndex] = originalPixel[globalIndex];

                }
                globalIndex +=1;

            }

        }
    }
    threadStat [threadNumber] = true;
}

void ThreadColoration::getAB(const QColor &colorTopaint, float &a, float &b)
{
    float X = (0.4124 * colorTopaint.red() + 0.3576 * colorTopaint.green() + 0.1805 * colorTopaint.blue()) * m_100D255;
    float Y = (0.2126 * colorTopaint.red() + 0.7152 * colorTopaint.green() + 0.0722 * colorTopaint.blue()) * m_100D255;
    float Z = (0.0193 * colorTopaint.red() + 0.1192 * colorTopaint.green() + 0.9505 * colorTopaint.blue()) * m_100D255;
    X = X / 94.811;
    Y = Y / 100.0;
    Z = Z / 107.304;

    if (X > 0.008856 )
        X = powf(X , m_oneDivthree);
    else
        X = ( 7.787037 * X ) + m_gama;

    if (Y > 0.008856 )
        Y =  powf(Y , m_oneDivthree);
    else
        Y = ( 7.787037 * Y ) + m_gama;

    if ( Z > 0.008856 )
        Z =  powf(Z , m_oneDivthree);
    else
        Z = ( 7.787037 * Z ) + m_gama;

    a = 500.0 * ( X - Y );
    b = 200.0 * ( Y - Z );
}
void ThreadColoration::getL(const QColor &imageColor, float &l)
{

    float Y = (0.2126 * imageColor.red() + 0.7152 * imageColor.green() + 0.0722 * imageColor.blue()) * m_100D255;

    Y = Y / 100.0;

    if (Y > 0.008856 )
        Y =  powf(Y , m_oneDivthree);
    else
        Y = ( 7.787037 * Y ) + m_gama;

   l = 116 * Y -16;

}
QColor ThreadColoration::rgbFromlab(float const &l, float const &a, float const &b)
{
    float Y = (l + 16) / 116;
    float X = a / 500 + Y;
    float Z = Y - b / 200;

    if ( Y  > m_gama )
        Y = powf(Y, 3);
    else
        Y = ( Y - m_gama ) / 7.787037;


    if ( X  > m_gama )
        X = powf(X, 3);
    else
        X = (X - m_gama ) / 7.787037;

    if ( Z  > m_gama )
        Z = powf(Z, 3);
    else
        Z = ( Z - m_gama ) / 7.787037;

    X = X * 94.811 * 2.55;
    Y = Y * 100.0 * 2.55;
    Z = Z * 107.304 * 2.55;

    int R = (int)roundf(X *  (3.2406) + Y * (-1.5372) + Z * (-0.4986));
    int G = (int)roundf(X * (-0.9689) + (Y *  1.8758) + Z *  0.0415);
    int B = (int)roundf(X *  0.0557 + Y * (-0.2040) + Z *  1.0570);

    if(R>255)
        R=255;
    if(G>255)
        G=255;
    if(B>255)
        B=255;

    if(R<0)
        R=0;
    if(G<0)
        G=0;
    if(B<0)
        B=0;
    return QColor(R,G,B);
}
