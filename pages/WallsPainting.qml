import QtQuick 2.11
import QtQuick.Layouts 1.6
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import Qt.labs.settings 1.0
import "../components"
import Paint 1.0
import QtGraphicalEffects 1.0


ScrollablePage {
    id: page
    width: stackView.height
    height: stackView.width
    anchors.centerIn: stackView

    RotationAnimator on rotation {
            from: 0;
            to: 90;

            duration: 200
        }



    Item{
        id: colBar
        height: parent.height
        width : parent.width/9 + 20

        Rectangle{
            anchors.fill:parent
            anchors.bottom: backbutton.top
            radius: 5
            LinearGradient {
                    anchors.fill: parent
                    start: Qt.point(0, 0)
                    end: Qt.point(parent.width, 0)
                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "white" }
                        GradientStop { position: 1.0; color: "#f0eaeaea" }
                    }
                }
        }

        ListView{
            id: choosenColorsListView
            spacing: 10
            width: parent.width
            height: parent.height * 3/5
            anchors.top: parent.top
            anchors.topMargin: 20
            clip: true

            model: ListModel{
                id: choosenColorsModel
                ListElement{
                    color:"#ffffff"
                    path:""
                    reference: "ral-9010"
                }

            }
            ButtonGroup {
                id: radioButtonGroup
            }

            delegate: Item{
                anchors.horizontalCenter: parent.horizontalCenter
                height: colorsDelegate.height + textDel.height + colorsDelegate.anchors.bottomMargin
                ItemDelegate{
                id: colorsDelegate
                property string path: model.path
                property string reference: model.reference

                anchors.horizontalCenter: parent.horizontalCenter
                width: stackView.width/8
                height: stackView.width/8
                ButtonGroup.group: radioButtonGroup
                checkable: true


                background: Rectangle{
                    width: colorsDelegate.width
                    height: colorsDelegate.width
                    radius: 200
                    color: model.color
                    border.color: colorsDelegate.checked ? "#000000" : "#aeadac"
                    border.width: 1

                    Image{
                        id: sourceI
                        fillMode: Image.PreserveAspectCrop
                        anchors.centerIn: parent
                        source: model.path + "-mini.png"
                        visible: false
                    }

                    Rectangle{
                        id:mask
                        anchors.fill: parent
                        radius: 200
                        visible: false
                    }

                    OpacityMask {
                            anchors.fill: mask
                            source: sourceI
                            maskSource: mask
                        }

                    Rectangle{
                        anchors.fill: parent
                        radius: 200
                        color: colorsDelegate.down ? "#601e1b18":"transparent"
                    }




                }


                onClicked: {
                    if(timer.running)
                    {
                        choosenColorsModel.remove(index)
                        timer.stop()
                    }else
                    {
                        timer.restart()
                        if(model.path !== "")
                        {
                            var str = model.path
                            str = str.replace("qrc", "")
                            drawItem.loadTexture(str)
                        }
                    }
                }

            }
                Text{
                    id: textDel
                    anchors.top: colorsDelegate.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.family: "Cordia New"
                    font.pointSize: 8
                    text: model.reference

                }

            }

    }

        Image {
            id: logo
            source: "qrc:/images/operaLogo.png"
            fillMode: Image.PreserveAspectFit
            width:parent.width
            anchors.bottom: colorsList.top
        }

        Button{
            id:backbutton
            text: langue.now["back"]
            anchors.bottom: parent.bottom
            width:parent.width

            background: Rectangle{
                gradient: Gradient {
                    GradientStop {
                        position: 0.0
                        color:"#d7d6d5"
                    }

                    GradientStop {
                        position: 0.05
                        color: !backbutton.pressed ? "white" : "#d7d6d5"
                    }
                    GradientStop {
                        position: 0.9
                        color: !backbutton.pressed ? "white" : "#d7d6d5"
                    }
                    /*GradientStop {
                        position: 1.0
                        color: !backbutton.pressed ? "gray": "darkgray"
                    }*/
            }
            }
            onClicked: {
                if(drawItem.visible)
                {
                    returnAnimation.running = true
                    sidebarGrid.interactive = true
                    colorInWall.visible = false;
                    var i
                    for(i=0; i<6; i++)
                    {
                        drawItem.children[i].visible = false
                    }
                }else
                {
                    escBack.activated()
                }
            }
        }
        Button{
            id:colorsList
            text: langue.now["colors"]
            anchors.bottom: backbutton.top
            width:parent.width

            background: Rectangle{
                gradient: Gradient {
                    GradientStop {
                        position: 0.0
                        color:"#d7d6d5"
                    }

                    GradientStop {
                        position: 0.05
                        color: !colorsList.pressed ? "white" : "#d7d6d5"
                    }
                    GradientStop {
                        position: 0.9
                        color: !colorsList.pressed ? "white" : "#d7d6d5"
                    }

                }
            }
            onClicked: {
                sidebarGrid.enabled = false
                palletOfColors.visible = true
            }
        }


    Component.onCompleted: {
        toolBar.visible = false;



    }
    }


    Item{
        id: palletOfColors
        TabBar{
        z:1
        id: paintType
        width: parent.width
        font.pixelSize: 10
        contentHeight: font.pixelSize + 20
        TabButton {
               id: ral
               text: qsTr("Ral")
               onClicked: {
                    colorsGrids.model = modelColorsLists.bRal
               }
           }
        TabButton {
               id: jungle
               text: qsTr(langue.now["jungle"])
               onClicked: {
                    colorsGrids.model = modelColorsLists.bJungle
               }
           }

        TabButton {
               id: decoration
               text: qsTr(langue.now["decorative"])
               onClicked: {
                    colorsGrids.model = modelColorsLists.bDecor
               }
           }
        }


        Timer{
            id:timer
            interval: 200
        }

        Rectangle{
            id: toolTip

            color: "#7035322f"
            visible: false
            width: toolTipText.contentWidth +5
            height: toolTipText.contentHeight + 5
            radius: 5
            z:100
            Text {
                id: toolTipText               
                anchors.centerIn: toolTip
                text: qsTr("Color chosen")
                color: "white"
            }
            PropertyAnimation {
                id: toolTipAnimation
                target: toolTip
                property: "opacity"
                to: 0.0
                duration: 1000
                onStopped: {
                    toolTip.visible = false
                }
            }
        }

        z:50


        width: imagesToChoose.width * 4/5
        height: imagesToChoose.height * 4/5
        Rectangle{
            color: "#f3e6e6"
            radius: 10
            anchors.fill: parent
        }


        x: (imagesToChoose.width - width) / 2 + colBar.width
        y: (imagesToChoose.height - height) / 2
        visible: false

        Column{
            id: colorsDialog
            anchors.top: paintType.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right


            spacing: 5

            GridView{
                id: colorsGrids
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width -20
                height: parent.height - stackView.width/8 -10
                cellWidth: width / 7
                cellHeight:height /3
                boundsBehavior: Flickable.DragOverBounds
                clip: true

                model: modelColorsLists.bRal

                delegate: modelColorsLists.bColorsChoicesItem

            }
            Rectangle{
                color: "#d7d6d5"
                width: parent.width
                height: 1
            }

            Button{
                id: okColorsButton 
                anchors.horizontalCenter: parent.horizontalCenter
                width: stackView.width/10
                height: stackView.width/10
                contentItem: Text{
                    text: qsTr("OK")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: "#80c342"

                }
                background: Rectangle{
                    radius: 100
                    color: okColorsButton.down ? "#d7d6d5" : "transparent"
                    border.color: "#80c342"
                    border.width: 1
                }
                onClicked: {
                    palletOfColors.visible = false
                    sidebarGrid.enabled = true
                }


            }


        }


    }

    Item{
        id: imagesToChoose
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: colBar.right
        anchors.right: parent.right

        ParallelAnimation
        {
            id: returnAnimation

            PropertyAnimation {
            target: drawItem;
            property: "width";
            to: drawItem.oldWidth;
            duration: 40
            }

            PropertyAnimation {
            target: drawItem;
            property: "height";
            to: drawItem.oldHeight;
            duration: 40
            }

            PropertyAnimation {
            target: drawItem;
            property: "x";
            to: drawItem.oldX;
            duration: 40
            }

            PropertyAnimation {
            target: drawItem;
            property: "y";
            to: drawItem.oldY;
            duration: 40
            }
            onStopped: {
                drawItem.visible = false
                sidebarGrid.enabled = true
            }

        }
        ParallelAnimation
        {
            id: fillAnimation

            PropertyAnimation {
            target: drawItem;
            property: "width";
            to: imagesToChoose.width;
            easing.type: Easing.InExpo;
            duration: 10
            }

            PropertyAnimation {
            target: drawItem;
            property: "height";
            to: imagesToChoose.height;
            easing.type: Easing.InExpo;
            duration: 10
            }

            PropertyAnimation {
            target: drawItem;
            property: "x";
            to: 0;
            easing.type: Easing.InExpo;
            duration: 10
            }

            PropertyAnimation {
            target: drawItem;
            property: "y";
            to: 0;
            easing.type: Easing.InExpo;
            duration: 10
            }
            onStopped: {
                drawItem.update()
                drawItem.loadWallsData(drawItem.width, drawItem.height)
                drawItem.enabled = true
            }

        }

        ImageToPaint{

            function showColorInWall(index) {
                colorInWall.visible = true;
                colorInWall.bWallColor = drawItem.children[index].wColor;
                colorInWall.bPath = drawItem.children[index].wPath;
                colorInWall.bIndex = index
                colorInWall.bReference = drawItem.children[index].wReference
                colorInWall.bWallNumber = drawItem.children[index].wWallNumber


                if(drawItem.children[index].x > drawItem.width / 2)
                {
                    colorInWall.x = drawItem.children[index].x - colorInWall.width -10;
                }
                else
                {
                    colorInWall.x = drawItem.children[index].x+ drawItem.children[index].width +10;
                }

                if(drawItem.children[index].y > drawItem.height / 2)
                {
                    colorInWall.y = drawItem.children[index].y -  colorInWall.height + drawItem.children[index].height;
                }else
                {
                    colorInWall.y = drawItem.children[index].y;
                }



           }

            id: drawItem
            path: ":/images/kitchen2"

            property int oldWidth: parent.width
            property int oldHeight: parent.height
            property int oldX: 0
            property int oldY: 0
            z:10
            visible: false
            MouseArea{
                id: mouseArea
                anchors.fill:parent
                onReleased: {
                    if(colorInWall.visible)
                    {
                        colorInWall.visible = false
                    }else
                    {
                        drawItem.paintWall(mouseX, mouseY, radioButtonGroup.checkedButton.background.color, radioButtonGroup.checkedButton.path);
                    }
                }

            }



            children :[
                PaintButton{
                    onClicked: {
                        drawItem.showColorInWall(0)
                    }
                },
                PaintButton{
                    onClicked: {
                         drawItem.showColorInWall(1)
                    }
                },
                PaintButton{
                    onClicked: {
                         drawItem.showColorInWall(2)
                    }
                },
                PaintButton{
                    onClicked: {
                        drawItem.showColorInWall(3)
                    }
                },
                PaintButton{
                    onClicked: {
                        drawItem.showColorInWall(4)
                    }
                },
                PaintButton{
                    onClicked: {
                        drawItem.showColorInWall(5)
                    }
                }
            ]


            ColorInWall{
                id: colorInWall
                width: parent.width/7
                height: width * 3/2
                onColorClicked: {

                    if(timer.running)
                    {
                        drawItem.removePainting(bWallNumber)
                        drawItem.children[bIndex].visible = false
                        visible = false
                        timer.stop()
                    }else
                    {
                        timer.restart()

                    }

                }
            }

            onWallColored: {
                var i;
                var found = false;
                for(i=0; i<6; i++)
                {
                    if(wallNmber === drawItem.children[i].wWallNumber)
                    {
                        drawItem.children[i].visible = true;
                        drawItem.children[i].x = x - drawItem.children[i].width/2;
                        drawItem.children[i].y = y - drawItem.children[i].height/2;
                        drawItem.children[i].wColor = radioButtonGroup.checkedButton.background.color;
                        drawItem.children[i].wPath = radioButtonGroup.checkedButton.path;
                        drawItem.children[i].wReference = radioButtonGroup.checkedButton.reference;
                        drawItem.children[i].wWallNumber = wallNmber;
                        found = true;
                        break;
                    }
                }

                if(!found)
                {
                    for(i=0; i<6; i++)
                    {
                        if(!drawItem.children[i].visible)
                        {
                            drawItem.children[i].visible = true;
                            drawItem.children[i].x = x - drawItem.children[i].width/2;
                            drawItem.children[i].y = y - drawItem.children[i].height/2;
                            drawItem.children[i].wColor = radioButtonGroup.checkedButton.background.color;
                            drawItem.children[i].wPath = radioButtonGroup.checkedButton.path;
                            drawItem.children[i].wReference = radioButtonGroup.checkedButton.reference;
                            drawItem.children[i].wWallNumber = wallNmber

                            break;
                        }
                    }
                }
            }

        }



        TabBar{
        id: roomsType
        width: parent.width
        font.pixelSize: 10
        contentHeight: font.pixelSize + 20
        TabButton {
               id: allTabButton
               text: qsTr(langue.now["all"])
               onClicked: {
                    sidebarGrid.model = modelRoomsLists.bAll
               }
           }
        TabButton {
           text: qsTr(langue.now["lounges"])
           onClicked: {
               sidebarGrid.model = modelRoomsLists.bLivingRooms
           }
        }
        TabButton {
           text: qsTr(langue.now["rooms"])
           onClicked: {
               sidebarGrid.model = modelRoomsLists.bRooms

           }
        }
        TabButton {
              text: qsTr(langue.now["kitchens"])
              onClicked: {
                  sidebarGrid.model = modelRoomsLists.bKitchens
              }
          }
        TabButton {
            text: qsTr(langue.now["facades"])
            onClicked: {
                sidebarGrid.model = modelRoomsLists.bFacades
            }
          }
        TabButton {
            text: qsTr(langue.now["bathrooms"])
            onClicked: {
                sidebarGrid.model = modelRoomsLists.bBathrooms
            }
          }
    }


        GridView {
             id: sidebarGrid

             anchors.top:roomsType.bottom
             anchors.left: parent.left
             anchors.bottom: parent.bottom
             width: parent.width
             clip: true


             cellWidth: width/3
             cellHeight:cellWidth * 2/3
             model: modelRoomsLists.bAll
             delegate:modelRoomsLists.bImageButtonDelegate
        }


    }


    ImageLists{
        id: modelRoomsLists
    }
    ColorsLists{
        id: modelColorsLists
    }






}
