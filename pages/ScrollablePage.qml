import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    id: page
    background: Rectangle{
        color: "transparent"
    }

    Flickable {
        anchors.fill: parent
        contentHeight: parent.height
        flickableDirection: Flickable.AutoFlickIfNeeded

        ScrollIndicator.vertical: ScrollIndicator { }
    }
}
