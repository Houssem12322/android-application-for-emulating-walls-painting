import QtQuick 2.11
import QtQuick.Controls 2.4
import QtWebView 1.1
import Qt.labs.settings 1.0
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.Universal 2.4
import QtQuick.Layouts 1.6

Page {
    id: page

    signal back
    signal forward
    WebView{
        id: webPage
        anchors.topMargin: toolBar.height + 3
        anchors.fill: parent
        url: "http://www.youtube.com/channel/UC2c9Jk-UVpn-yHSxxVIHrTg"
        BusyIndicator {
            anchors.centerIn: parent
        }

    }
    onBack: {
        webPage.goBack()
    }
    onForward: {
        webPage.goForward()
    }

}

