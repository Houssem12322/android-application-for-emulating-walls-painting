import QtQuick 2.11
import QtQuick.Controls 2.4
import QtLocation 5.11
import QtPositioning 5.11
import QtQuick.Layouts 1.11
import QtQuick.Controls.Material 2.4
//import QtQuick.Controls.Universal 2.4
import Qt.labs.settings 1.0

ScrollablePage {
    id: page

    Plugin {
            id: mapPlugin
            name: "osm" // "mapboxgl", "esri", ...
            // specify plugin parameters if necessary
            // PluginParameter {
            //     name:
            //     value:
            // }
        }
    Item{
        id: map
        anchors.fill: parent;
        anchors.margins: 10
        anchors.bottomMargin: parent.height/3

        Map {
            anchors.fill: parent
            anchors.margins: 5
            plugin: mapPlugin
            center: QtPositioning.coordinate(35.537555, 6.136004) // Opera painture
            zoomLevel: 14
            copyrightsVisible: false


            MapQuickItem {
                id: marker
                anchorPoint.x: image.width/4
                anchorPoint.y: image.height
                coordinate: QtPositioning.coordinate(35.537555, 6.136004)

                sourceItem: Image {
                    id: image
                    source: "qrc:/icons/marker.png"
                }
            }


       }
    }

    ColumnLayout{
        id: titles
        layoutDirection: (langue.now["lang"]=== "ar")? Qt.RightToLeft:Qt.LeftToRight
        anchors.right: (langue.now["lang"]=== "ar")? map.right:undefined
        anchors.left: (langue.now["lang"]=== "ar")? undefined:map.left
        anchors.top: map.bottom
        anchors.margins: 10
        spacing: 8
        Text {

            color: "#5d5b59"
            text: qsTr(langue.now["adress"] + "\n")
            font.family: "Caladea"
            font.pixelSize: 15
            verticalAlignment: Text.AlignVCenter
        }

        Text {

            color: "#5d5b59"
            text: qsTr(langue.now["e-mail"])
            font.family: "Caladea"
            font.pixelSize: 15
            verticalAlignment: Text.AlignVCenter
        }
        Text {

            color: "#5d5b59"
            text: qsTr(langue.now["phone"] + "\n")
            font.family: "Caladea"
            font.pixelSize: 15
            verticalAlignment: Text.AlignVCenter
        }

        Text {

            color: "#5d5b59"
            text: qsTr(langue.now["website"])
            font.family: "Caladea"
            font.pixelSize: 15
            verticalAlignment: Text.AlignVCenter
        }
        ToolButton {
            Material.foreground: "#757575"
            Material.background: "#ecaaaaaa"
            icon.name: "back"
            onClicked: {
                escBack.activated()
            }
        }

    }

    ColumnLayout{
        id:info
        anchors.top: map.bottom
        anchors.margins: 10
        spacing: 8
        layoutDirection: (langue.now["lang"]=== "ar")? Qt.RightToLeft:Qt.LeftToRight

        anchors.right: (langue.now["lang"]=== "ar")? titles.left:undefined
        anchors.left: (langue.now["lang"]=== "ar")? undefined:titles.right


        Text {

            color: "#5d5b59"
            text: qsTr(langue.now["adrInfo"])
            font.family: "Caladea"
            font.pixelSize: 15
            verticalAlignment: Text.AlignVCenter
        }

        Text {

            color: "#5d5b59"
            text: qsTr("opera_paint@yahoo.fr")
            font.family: "Caladea"
            font.pixelSize: 15
            verticalAlignment: Text.AlignVCenter
        }

        Text {

            color: "#5d5b59"
            text: qsTr("+213 773 884767\n+213 551 126361")
            font.family: "Caladea"
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 15
        }

        Text {

            text: qsTr('<a href="http://operapeinture.com">http://operapeinture.com</a>')
            onLinkActivated: Qt.openUrlExternally(link)
            font.family: "Caladea"
            verticalAlignment: Text.AlignVCenter


        }

    }


   Component.onCompleted: {
        toolBar.visible = false;


    }

}
