  #include "walls.h"

Walls::Walls()
{

}

void Walls::loadData (const QString &path, int wallNumber)
{

    QFile anchorsFile(path + "-fw" + QString::number(wallNumber) + ".txt");
    m_exists = anchorsFile.exists();
    if(m_exists)
    {

        if (!anchorsFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            printf("error when reading anchord file");
            exit(1);
        }
        QTextStream in(&anchorsFile);
        in>>m_x>>m_y>>m_rightLimit>>m_bottomLimit>>m_transformation[0]>>m_transformation[1]>>m_transformation[2]
                >>m_transformation[3]>>m_transformation[4]>>m_transformation[5]>>m_transformation[6]>>m_transformation[7]
                >>m_transformation[8]>>m_transformation[9]>>m_transformation[10];

        if(!load(path + "-w" + QString::number(wallNumber) + ".png", "PNG"))
        {
            printf("error when loading wall image");
            exit(1);
        }
    }




}


void Walls::fillInImage(QImage &img, QImage &copy, const QColor &paintColor)
{
    for(int i=0; i<threadNumber; i++)
    {
         m_parallelProcess[i].set(&img, this, &copy , paintColor, i * (int)((height()+threadNumber) /threadNumber),
                                  m_y, m_x, int((height()+threadNumber)/threadNumber), i, m_threadStats);
         m_parallelProcess[i].start();
    }
    bool finish = false;
    while(!finish)
    {
        for(int j=0; j<threadNumber; j++)
        {
            if(!m_threadStats[j])
            {
                finish = false;
                break;
            }
            else
            {
                finish = true;
            }
        }
    }

    for(int i=0; i<threadNumber; i++)
    {
        m_threadStats[i] = false;
        m_parallelProcess[i].quit();
        m_parallelProcess[i].wait();
    }

}

bool Walls::fillTexure(QImage &img, QImage &texture, QImage &copy)
{
    QImage transformedImage = texture.transformed(QTransform(m_transformation[0], m_transformation[1]
            , m_transformation[2], m_transformation[3], m_transformation[4], m_transformation[5]
            , m_transformation[6], m_transformation[7], m_transformation[8]));
    if(m_transformation[8] == 1.0){
        for(int i=0; i<threadNumber; i++)
        {

            m_parallelProcess[i].setTexture(&img, this, &transformedImage, &copy, i * (int)((height()+threadNumber) /threadNumber),
                                            m_y, m_x, int((height()+threadNumber)/threadNumber), i, m_threadStats, m_transformation);
            m_parallelProcess[i].start();
        }
        bool finish = false;
        while(!finish)
        {
            for(int j=0; j<threadNumber; j++)
            {
                if(!m_threadStats[j])
                {
                    finish = false;
                    break;
                }
                else
                {
                    finish = true;
                }
            }
        }

        for(int i=0; i<threadNumber; i++)
        {
            m_threadStats[i] = false;
            m_parallelProcess[i].quit();
            m_parallelProcess[i].wait();
        }
        return true;

    }else
    {
        return false;
    }
}

void Walls::removePainting(QImage &img, QImage &copy)
{
    for(int i=0; i<threadNumber; i++)
    {

         m_parallelProcess[i].setToRemovePainting(&img, this, &copy, i * (int)((height()+threadNumber) /threadNumber),
                                  m_y, m_x, int((height()+threadNumber)/threadNumber), i, m_threadStats);
         m_parallelProcess[i].start();
    }
    bool finish = false;
    while(!finish)
    {
        for(int j=0; j<threadNumber; j++)
        {
            if(!m_threadStats[j])
            {
                finish = false;
                break;
            }
            else
            {
                finish = true;
            }
        }
    }

    for(int i=0; i<threadNumber; i++)
    {
        m_threadStats[i] = false;
        m_parallelProcess[i].quit();
        m_parallelProcess[i].wait();
    }
}

bool Walls::inWall(int x, int y)
{
    if(!isNull())
    {
        if(x>m_x && x<m_rightLimit && y>m_y && y<m_bottomLimit)
        {
            if(pixelColor(x-m_x, y-m_y).alpha() > 0)
            {
                return true;
            }
        }
    }
    else
    {
        return false;
    }
    return false;
}
